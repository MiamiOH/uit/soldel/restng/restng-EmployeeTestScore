<?php

return [
    'resources' => [
        'employee' => [
            MiamiOH\RestngEmployeeTestScore\Resources\EmployeeTestScoreResourceProvider::class,
            MiamiOH\RestngEmployeeTestScore\Resources\TerminatedEmployeeResourceProvider::class,
            MiamiOH\RestngEmployeeTestScore\Resources\EmployeeNewHireResourceProvider::class,
            MiamiOH\RestngEmployeeTestScore\Resources\EmployeeSupervisorResourceProvider::class
        ],

    ]
];