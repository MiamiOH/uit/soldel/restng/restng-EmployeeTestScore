<?php

namespace MiamiOH\RestngEmployeeTestScore\Tests\Unit;

class PutInputValidationTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $testScore;
    private $api;
    private $request;
    private $dbh;
    private $sth;
    private $user;

    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam', 'callResource'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\DBH\OCI8')
            ->setMethods(array('queryall_array',
                'prepare',
                'queryfirstcolumn'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized', 'isAuthenticated'))
            ->getMock();

        $this->db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\STH\OCI8')
            ->disableOriginalConstructor()
            ->setMethods(array('execute', 'fetchrow_assoc'))
            ->getMock();

        $this->db->method('getHandle')->willReturn($this->dbh);
        $this->dbh->method('prepare')->willReturn($this->sth);
        $this->sth->method('execute')->willReturn(true);

        $this->user->method('isAuthorized')->willReturn(true);
        $this->user->method('isAuthenticated')->willReturn(true);

        $this->testScore = new \MiamiOH\RestngEmployeeTestScore\Services\EmployeeTestScore();
        $this->testScore->setApiUser($this->user);
        $this->testScore->setDatabase($this->db);
        $this->testScore->setRequest($this->request);
    }

    private function runOK($data)
    {
        $this->request
            ->expects($this->any())
            ->method('getData')
            ->will($this->returnValue($data));

        $resp = $this->testScore->putTestScores();

        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $resp->getStatus());
    }

    // private function runBadData($requestData, $failData, $errKeys, $optional = '')
    // {
    //     /*
    //       This is test for those error that is catched at run time. Therefore, there is no exception. 
    //     */
    //     $this->request
    //         ->expects($this->any())
    //         ->method('getData')
    //         ->will($this->returnValue($requestData));

    //     $payload = array('errors' => array());
    //     foreach ($failData as $datum) {
    //         $datum['error_message'] = $this->testScore->getErrMesg($errKeys[$index], $optional);
    //     }

    //     $resp = $this->testScore->putTestScores();
    //     $failData['error_message'] = $this->testScore->getErrMesg($errKey, $optional);

    //     $this->assertEquals(MiamiOH\RESTng\App::API_BADREQUEST, $resp->getStatus());
    //     $this->assertEquals(array('errors' => array($failData)), $resp->getPayLoad());
    // }

    private function runBadData($requestData, $failData, $errKeys)
    {
        /*
          This is test for those error that is catched at run time. Therefore, there is no exception. 
        */
        $this->request
            ->expects($this->any())
            ->method('getData')
            ->will($this->returnValue($requestData));

        $payload = array('errors' => array());

        $index = 0;
        foreach ($failData as $datum) {
            $datum['error_message'] = $this->testScore->getErrMesg($errKeys[$index]);
            array_push($payload['errors'], $datum);
            $index += 1;
        }

        $resp = $this->testScore->putTestScores();

        $this->assertEquals(\MiamiOH\RESTng\App::API_BADREQUEST, $resp->getStatus());
        $this->assertEquals($payload, $resp->getPayLoad());
    }

    private function runException($data, $errKey, $optional = '')
    {
        try {
            $this->request
                ->expects($this->any())
                ->method('getData')
                ->will($this->returnValue($data));

            $resp = $this->testScore->putTestScores();
            //If there is not Exception Throw, the test is fail
            $this->assertTrue(false, 'There is no Exception Throw');
        } catch (\Exception $e) {
            $this->assertEquals($this->testScore->getErrMesg($errKey, $optional), $e->getMessage());
        }
    }

    public function testNoDataNegative()
    {
        $data = array();

        $this->runException('', 'noData');
    }

    public function testDefaultPositive()
    {
        $data = array(
            array('uniqueID' => 'doqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
                'examScore' => 0,
                'examPercent' => 0,
            ),
            array('pidm' => '187663',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-22T00:16:20',
                'examScore' => 0,
                'examPercent' => 0,
            )

        );
        $this->runOK($data);
    }

    public function testDefaultNegative()
    {
        $data = array(
            array('uniqueID' => 'doqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
                'examScore' => 0,
                'examPercent' => 0,
            ),
            array('pidm' => '000000',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-22T00:16:20',
                'examScore' => 0,
                'examPercent' => 0,
            ),
            array('uniqueID' => 'beckmd',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-2200:16:20',
                'examScore' => 0,
                'examPercent' => 0,
            )
        );

        $this->dbh
            ->method('queryfirstcolumn')
            ->will($this->onConsecutiveCalls('999999', DB_EMPTY_SET, '187663'));

        $this->runBadData($data, array($data[1], $data[2]), array('noPidm', 'invalidExamDate'));
    }

    public function testPidmPositiveA()
    {
        $data = array(
            array('pidm' => '187663',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T20:16:20',
            )
        );
        $this->runOK($data);
    }

    public function testPidmPositiveB()
    {
        $data = array(
            array('pidm' => '15994532',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T01:16:20',
            )
        );
        $this->runOK($data);
    }

    public function testMutiplePidmPositive()
    {
        # This should be true because hash only returns one unique key even if mutiple duplicate keys are provided.
        $data = array(
            array('pidm' => '15994532',
                'pidm' => '187663',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T01:16:20',
            )
        );
        $this->runOK($data);
    }

    public function testPlusNumberPositiveA()
    {
        $data = array(
            array('plusNumber' => '+01578180',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T02:16:20',
            )
        );
        $this->runOK($data);
    }

    public function testPidmNegativeA()
    {
        $data = array(
            array('pidm' => '159945321',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidPidm'));
    }

    public function testPidmNegativeB()
    {
        $data = array(
            array('pidm' => 'doqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidPidm'));
    }

    public function testPlusNumberNegativeA()
    {
        $data = array(
            array('plusNumber' => '12345678',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidPlusNumber'));
    }

    public function testPlusNumberNegativeB()
    {
        $data = array(
            array('plusNumber' => 'doqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidPlusNumber'));
    }

    public function testPlusNumberNegativeC()
    {
        $data = array(
            array('plusNumber' => '+123456',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-07-01T12:00:00',
            ));
        $this->dbh
            ->expects($this->exactly(1))
            ->method('queryfirstcolumn')
            ->will($this->onConsecutiveCalls(DB_EMPTY_SET));

        $this->runBadData($data, array($data[0]), array('noPidm'));
    }

    public function testInvalidUniqueIDNegative()
    {
        $data = array(
            array('uniqueID' => 'magimap23',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidUniqueID'));
    }

    /*
      -------------------------------
      Exam Score Test
      -------------------------------
    */
    public function testNoExamScoreNegative()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'examCode' => '01',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('noExamScore'));
    }

    public function testTwoExamScorePositive()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'pass' => 'P',
                'examScore' => '95.18',
                'examCode' => '01',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runOK($data);
    }

    public function testThreeExamScorePositive()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'pass' => 'P',
                'examScore' => '95.18',
                'examPercent' => '70.55',
                'examCode' => '01',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runOK($data);
    }

    public function testExamScorePositiveA()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'pass' => 'P',
                'examScore' => '95.188',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamScore'));
    }

    /*
      -------------------------------
      Exam Code Test
      -------------------------------
    */
    public function testNoExamCodeNegative()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:59',
            )
        );
        $this->runBadData($data, array($data[0]), array('noExamCode'));
    }

    /*
      -------------------------------
      Mutiple or no IDs test
      -------------------------------
    */
    public function testNoIDNegative()
    {
        $data = array(
            array('examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('extraIDs'));
    }

    public function testWithTwoIDsNegative()
    {
        $data = array(
            array('uniqueID' => 'doqh',
                'pidm' => '123456',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('extraIDs'));
    }

    public function testWithThreeIDsNegative()
    {
        $data = array(
            array('uniqueID' => 'doqh',
                'pidm' => '123456',
                'plusNumber' => '+123456',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('extraIDs'));
    }

    /*
      -------------------------------
      No Pattern Test
      -------------------------------
    */
    public function testNoPatternNegative()
    {
        $data = array(
            array(
                'id' => 'doqh',
            )
        );
        $this->runException($data, 'noPattern', 'id');
    }

    /*
      -------------------------------
      Date Format Test
      -------------------------------
    */
    public function testWrongDateFormatNegativeA()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20.960',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testWrongDateFormatNegativeB()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T1:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testWrongDateFormatNegativeC()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T01:1:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testWrongDateFormatNegativeD()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T01:00:0',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testWrongDateFormatNegativeE()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T-12:00:00',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testWrongDateFormatNegativeF()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T12:-13:00',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testWrongDateFormatNegativeG()
    {
        $data = array(
            array('uniqueID' => 'doqhdoqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T12:13:-30',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testDatePositiveA()
    {
        $data = array(
            array('plusNumber' => '+01578180',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '1990-02-28T00:00:00',
            )
        );
        $this->runOK($data);
    }

    public function testDateNegativeA()
    {
        $data = array(
            array('plusNumber' => '+01578180',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-02-30T01:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testDateNegativeB()
    {
        $data = array(
            array('plusNumber' => '+01578180',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => date('Y-m-d\TH:i:s', strtotime(' + 1 day')),
            )
        );
        $this->runBadData($data, array($data[0]), array('exceedCurrentDay'));
    }

    public function testDateNegativeC()
    {
        $data = array(
            array('plusNumber' => '+01578180',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-23T01:60:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testDateNegativeD()
    {
        $data = array(
            array('plusNumber' => '+01578180',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-23T01:59:60',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testDateNegativeE()
    {
        $data = array(
            array('plusNumber' => '+01578180',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T24:16:20',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidExamDate'));
    }

    public function testDateNegativeF()
    {
        $data = array(
            array('plusNumber' => '+01578180',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '1899-02-28T00:00:00',
            )
        );
        $this->runBadData($data, array($data[0]), array('invalidYear'));
    }

    public function testNoDateNegative()
    {
        $data = array(
            array('plusNumber' => '+01578180',
                'examCode' => '01',
                'pass' => 'P',
            )
        );
        $this->runBadData($data, array($data[0]), array('noExamDate'));
    }
}
