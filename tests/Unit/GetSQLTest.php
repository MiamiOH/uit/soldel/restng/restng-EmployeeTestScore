<?php

namespace MiamiOH\RestngEmployeeTestScore\Tests\Unit;

class GetSQLTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $testScore;
    private $api;
    private $request;
    private $dbh;
    private $sth;
    private $user;

    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam', 'callResource'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam',
                'getOptions',
                'getData',
                'isPartial',
                'getPartialRequestFields'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\DBH\OCI8')
            ->setMethods(array('queryall_array',
                'prepare',
                'queryfirstcolumn',
                'queryfirstrow_assoc'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized', 'isAuthenticated', 'getUsername'))
            ->getMock();

        $this->db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\STH\OCI8')
            ->disableOriginalConstructor()
            ->setMethods(array('execute', 'fetchrow_assoc'))
            ->getMock();

        $this->db->method('getHandle')->willReturn($this->dbh);
        $this->dbh->method('prepare')->willReturn($this->sth);
        $this->sth->method('execute')->willReturn(true);
        $this->user->method('isAuthorized')->willReturn(true);
        $this->user->method('isAuthenticated')->willReturn(true);

        $this->testScore = new \MiamiOH\RestngEmployeeTestScore\Services\EmployeeTestScore();
        $this->testScore->setApiUser($this->user);
        $this->testScore->setDatabase($this->db);
        $this->testScore->setRequest($this->request);
    }

    public function testPartialFieldsCompletePositive()
    {
        $this->dbh
            ->method('prepare')
            ->with($this->callback(
                function ($arg) {
                    return substr_count(
                            $arg,
                            "SELECT szbuniq_unique_id, pprexam_pidm, spriden_id, pprexam_exam_code, " .
                            "ptrexam_desc, TO_CHAR(pprexam_exam_date, 'yyyy-mm-dd\"T\"HH24:MI:SS') " .
                            "as pprexam_exam_date, pprexam_exam_score, pprexam_exam_percent, pprexam_pass_ind FROM"
                        ) === 1 && substr_count(
                            $arg,
                            "SELECT szbuniq_unique_id, pprexam_pidm, spriden_id, " .
                            "pprexam_exam_code, ptrexam_desc, pprexam_exam_date, " .
                            "pprexam_exam_score, pprexam_exam_percent, pprexam_pass_ind FROM"
                        ) === 1;
                }
            ));
        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }

    public function testPartialFieldsPositiveA()
    {
        $this->request
            ->method('getPartialRequestFields')
            ->will($this->returnValue(array('pidm')));

        $this->request
            ->method('isPartial')
            ->will($this->returnValue(true));

        $this->dbh
            ->method('prepare')
            ->with($this->callback(function ($arg) {
                return substr_count($arg, 'SELECT pprexam_pidm FROM') === 2;
            }));
        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }

    public function testPartialFieldsPositiveB()
    {
        $this->request
            ->method('getPartialRequestFields')
            ->will($this->returnValue(array('examCode',
                'examDescription')));

        $this->request
            ->method('isPartial')
            ->will($this->returnValue(true));

        $this->dbh
            ->method('prepare')
            ->with($this->callback(function ($arg) {
                return substr_count($arg, 'SELECT pprexam_exam_code, ptrexam_desc FROM') === 2;
            }));
        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }

    public function testDisplayPositiveA()
    {
        $options = array(
            'uniqueID' => array(
                'doqh'
            ),
            'pidm' => array(
                '237663'
            ),
            'plusNumber' => array(
                '+1234567'
            ),
            'examCode' => array(
                '01',
                '02',
                '03'
            ),
            'display' => 'highest',
        );
        $this->request
            ->method('getOptions')
            ->will($this->returnValue($options));
        $this->dbh
            ->method('queryall_array')
            ->will($this->onConsecutiveCalls(
                array(
                    array('pidm' => 999999),
                ),
                array(
                    array('pidm' => 187663),
                )
            ));

        $this->dbh
            ->method('prepare')
            ->with($this->callback(
                function ($arg) {
                    return substr_count(
                            $arg,
                            'pprexam_exam_score = (SELECT MAX(pprexam_exam_score) ' .
                            'FROM payroll.pprexam WHERE 1=1 AND pprexam_pidm IN (?,?,?) ' .
                            'AND pprexam_exam_code IN (?,?,?))'
                        ) === 1;
                }
            ));

        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }

    public function testDisplayPositiveB()
    {
        $options = array(
            'uniqueID' => array(
                'doqh'
            ),
            'pidm' => array(
                '237663'
            ),
            'plusNumber' => array(
                '+1234567'
            ),
            'examCode' => array(
                '01',
                '02',
                '03'
            ),
            'display' => 'lowest',
        );
        $this->request
            ->method('getOptions')
            ->will($this->returnValue($options));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));
        $this->dbh
            ->method('prepare')
            ->with($this->callback(
                function ($arg) {
                    return substr_count(
                            $arg,
                            'pprexam_exam_score = (SELECT MIN(pprexam_exam_score) ' .
                            'FROM payroll.pprexam WHERE 1=1 AND pprexam_pidm IN (?) ' .
                            'AND pprexam_exam_code IN (?,?,?))'
                        ) === 1;
                }
            ));

        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }

    public function testDisplayPositiveC()
    {
        $options = array(
            'uniqueID' => array(
                'doqh'
            ),
            'pidm' => array(
                '237663'
            ),
            'plusNumber' => array(
                '+1234567'
            ),
            'examCode' => array(
                '01',
                '02',
                '03'
            ),
            'display' => 'latest',
        );
        $this->request
            ->method('getOptions')
            ->will($this->returnValue($options));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));
        $this->dbh
            ->method('prepare')
            ->with($this->callback(
                function ($arg) {
                    return substr_count(
                        $arg,
                        'pprexam_exam_date = (SELECT MAX(pprexam_exam_date) ' .
                        'FROM payroll.pprexam WHERE 1=1 AND pprexam_pidm IN (?) ' .
                        'AND pprexam_exam_code IN (?,?,?)))'
                    );
                }
            ));

        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }
}
