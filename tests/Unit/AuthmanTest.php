<?php

namespace MiamiOH\RestngEmployeeTestScore\Tests\Unit;

class AuthmanTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $testScore;
    private $api;
    private $request;
    private $dbh;
    private $sth;
    private $user;

    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam', 'callResource'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\DBH\OCI8')
            ->setMethods(array('queryall_array',
                'prepare',
                'queryfirstcolumn'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized', 'isAuthenticated', 'getUsername'))
            ->getMock();

        $this->db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\STH\OCI8')
            ->disableOriginalConstructor()
            ->setMethods(array('execute', 'fetchrow_assoc'))
            ->getMock();

        $this->db->method('getHandle')->willReturn($this->dbh);
        $this->dbh->method('prepare')->willReturn($this->sth);
        $this->sth->method('execute')->willReturn(true);
        // $this->user->method('isAuthorized')->willReturn(true);
        $this->user->method('isAuthenticated')->willReturn(true);

        $this->testScore = new \MiamiOH\RestngEmployeeTestScore\Services\EmployeeTestScore();
        $this->testScore->setApiUser($this->user);
        $this->testScore->setDatabase($this->db);
        $this->testScore->setRequest($this->request);
    }

    public function testPutAuthManPositive()
    {
        $data = array(
            array('uniqueID' => 'doqh',
                'examCode' => '01',
                'pass' => 'P',
                'examDate' => '2016-06-17T17:16:20',
                'examScore' => 0,
                'examPercent' => 0,
            ),
        );

        $this->request
            ->expects($this->any())
            ->method('getData')
            ->will($this->returnValue($data));

        $this->user
            ->method('isAuthorized')
            ->with(
                $this->equalTo('WebServices'),
                $this->equalTo('EmployeeTestScore'),
                $this->callback(array($this, 'verifyPermsForUpsert'))
            )
            ->will($this->returnValue(true));
        $resp = $this->testScore->putTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $resp->getStatus());
    }

    // callback function must be public
    public function verifyPermsForUpsert($requestPerms)
    {
        $allowedPerms = array('create', 'update', 'edit', 'all');
        foreach ($requestPerms as $perm) {
            if (in_array($perm, $allowedPerms)) {
                return true;
            }
        }
        return false;
    }

    public function testPutAuthManNegative()
    {
        $this->user
            ->method('isAuthorized')
            ->will($this->returnValue(false));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));
        $this->sth
            ->method('fetchrow_assoc')
            ->will($this->onConsecutiveCalls($this->returnCallBack(array($this, 'mockFetchRow'), array())));
        $resp = $this->testScore->putTestScores();

        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
    }

    public function testGetAuthManPositive()
    {
        $this->user
            ->method('isAuthorized')
            ->with(
                $this->equalTo('WebServices'),
                $this->equalTo('EmployeeTestScore'),
                $this->callback(array($this, 'verifyPermsForUpsert'))
            )
            ->will($this->returnValue(true));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));
        $this->sth
            ->method('fetchrow_assoc')
            ->will($this->onConsecutiveCalls($this->returnCallBack(array($this, 'mockFetchRow')), array()));
        $resp = $this->testScore->getTestScores();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    }

    public function testGetAuthManNegative()
    {
        $this->user
            ->method('isAuthorized')
            ->will($this->returnValue(false));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));
        $this->sth
            ->method('fetchrow_assoc')
            ->will($this->onConsecutiveCalls($this->returnCallBack(array($this, 'mockFetchRow')), array()));
        $resp = $this->testScore->getTestScores();

        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
    }

    // callback function must be public
    public function verifyPermsForGet($requestPerms)
    {
        $allowedPerms = array('read', 'view', 'all');
        foreach ($requestPerms as $perm) {
            if (in_array($perm, $allowedPerms)) {
                return true;
            }
        }
        return false;
    }

    public function mockFetchRow()
    {
        return array(
            'szbuniq_unique_id' => 'doqh',
            'pprexam_pidm' => '999999',
            'spriden_id' => '+12345678',
            'pprexam_exam_code' => '01',
            'ptrexam_desc' => 'WeComply Title IX',
            'pprexam_exam_date' => '12-JUL-16',
            'pprexam_exam_score' => '98',
            'pprexam_exam_percent' => '98',
            'pprexam_pass_ind' => 'F',
        );
    }
}
