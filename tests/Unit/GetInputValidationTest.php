<?php

namespace MiamiOH\RestngEmployeeTestScore\Tests\Unit;

class GetInputValidationTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $testScore;
    private $api;
    private $request;
    private $dbh;
    private $sth;
    private $user;

    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam', 'callResource'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(
                array('getResourceParam',
                    'getOptions',
                    'getData',
                    'isPartial',
                    'getPartialRequestFields')
            )
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\DBH\OCI8')
            ->setMethods(array('queryall_array',
                'prepare',
                'queryfirstcolumn'))
            ->getMock();

        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized', 'isAuthenticated', 'getUsername'))
            ->getMock();

        $this->db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\STH\OCI8')
            ->disableOriginalConstructor()
            ->setMethods(array('execute', 'fetchrow_assoc'))
            ->getMock();

        $this->db->method('getHandle')->willReturn($this->dbh);
        $this->dbh->method('prepare')->willReturn($this->sth);
        $this->sth->method('execute')->willReturn(true);
        $this->user->method('isAuthorized')->willReturn(true);
        $this->user->method('isAuthenticated')->willReturn(true);

        $this->testScore = new \MiamiOH\RestngEmployeeTestScore\Services\EmployeeTestScore();
        $this->testScore->setApiUser($this->user);
        $this->testScore->setDatabase($this->db);
        $this->testScore->setRequest($this->request);
    }

    public function testOptionsDefaultPositive()
    {
        $options = array(
            'pidm' => array(
                '187663',
                '237663'
            ),
            'uniqueID' => array(
                'doqh',
                'beckmd'
            ),
            'plusNumber' => array(
                '+1234567'
            ),
            'examCode' => array(
                '01',
                '02',
                '03'
            ),
            'pass' => 'P',
            'display' => 'all',
        );
        $this->request
            ->method('getOptions')
            ->will($this->returnValue($options));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));
        $this->sth
            ->method('fetchrow_assoc')
            ->will($this->onConsecutiveCalls($this->returnCallBack(array($this, 'mockFetchRowA')), array()));

        $resp = $this->testScore->getTestScores();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    }

    public function testOptionsPidmNegative()
    {
        $options = array(
            'uniqueID' => array(
                'doqh'
            ),
            'pidm' => array(
                '187663a',
                '237663'
            ),
            'plusNumber' => array(
                '+1234567'
            ),
            'examCode' => array(
                '01',
                '02',
                '03'
            ),
            'display' => 'all',
        );
        $this->request
            ->method('getOptions')
            ->will($this->returnValue($options));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));

        try {
            $resp = $this->testScore->getTestScores();
            $this->assertTrue(false, 'There is no Exception Throw');
        } catch (\Exception $e) {
            $this->assertEquals($this->testScore->getErrMesg('invalidPidm'), $e->getMessage());
        }
    }

    public function testEmptyReturnNegative()
    {
        $this->sth
            ->method('fetchrow_assoc')
            ->will($this->returnValue(array()));
        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }

    public function testFieldExchangePositive()
    {
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));
        $this->sth
            ->method('fetchrow_assoc')
            ->will($this->onConsecutiveCalls($this->returnCallBack(array($this, 'mockFetchRowA')), array()));

        $resp = $this->testScore->getTestScores();
        $payload = $resp->getPayload();

        $expectedReturn = array(
            array(
                'uniqueID' => 'beckmd',
                'pidm' => '187663',
                'plusNumber' => '+00187663',
                'examCode' => '01',
                'examDescription' => 'WeComply PDH Employees',
                'examDate' => '11-JUL-16',
                'examScore' => '98',
                'examPercent' => '98',
                'pass' => 'P',
            )
        );

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertEquals($expectedReturn, $resp->getPayload());
    }

    public function mockFetchRowA()
    {
        return array(
            'szbuniq_unique_id' => 'beckmd',
            'pprexam_pidm' => '187663',
            'spriden_id' => '+00187663',
            'pprexam_exam_code' => '01',
            'ptrexam_desc' => 'WeComply PDH Employees',
            'pprexam_exam_date' => '11-JUL-16',
            'pprexam_exam_score' => '98',
            'pprexam_exam_percent' => '98',
            'pprexam_pass_ind' => 'P',
        );
    }

    public function mockFetchRowB()
    {
        return array(
            'szbuniq_unique_id' => 'doqh',
            'pprexam_pidm' => '999999',
            'spriden_id' => '+12345678',
            'pprexam_exam_code' => '01',
            'ptrexam_desc' => 'WeComply Title IX',
            'pprexam_exam_date' => '12-JUL-16',
            'pprexam_exam_score' => '98',
            'pprexam_exam_percent' => '98',
            'pprexam_pass_ind' => 'F',
        );
    }

    public function testInvalidPartialFieldNegative()
    {
        $this->request
            ->method('isPartial')
            ->will($this->returnValue(true));

        $this->request
            ->method('getPartialRequestFields')
            ->will($this->returnValue(array('examCde',
                'examDesiption')));

        try {
            $resp = $this->testScore->getTestScores();
            $this->assertTrue(false, 'There is no Exception Throw');
        } catch (\Exception $e) {
            $this->assertEquals($this->testScore->getErrMesg('invalidField'), $e->getMessage());
        }
    }

    public function testOneValidUniqueIDPositve()
    {
        $options = array(
            'uniqueID' => array(
                'doqh',
            ),
            'examCode' => array(
                '01',
            ),
            'display' => 'all',
        );
        $this->request
            ->method('getOptions')
            ->will($this->returnValue($options));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array(
                array('pidm' => '999999',
                    'unique_id' => 'doqh'))));
        $this->sth
            ->method('fetchrow_assoc')
            ->will($this->onConsecutiveCalls($this->returnCallBack(array($this, 'mockFetchRowB')), array()));

        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    }

    public function testOneInvalidUniqueIDNegative()
    {
        // Return 404 if an invalid id does not match any pidm
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));

        $options = array(
            'uniqueID' => array(
                'doq',
            ),
            'examCode' => array(
                '01',
            ),
            'pass' => 'F',
            'display' => 'all',
        );

        $this->request
            ->method('getOptions')
            ->will($this->returnValue($options));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));
        // Make sure API_NOTFOUND is returned
        $this->sth
            ->method('fetchrow_assoc')
            ->will($this->onConsecutiveCalls($this->returnCallBack(array($this, 'mockFetchRow_B')), array()));

        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }

    public function testOneInvalidPlusNumberNegative()
    {
        // Return 404 if an invalid id does not match any pidm
        $options = array(
            'plusNumber' => array(
                '+123456',
            ),
            'examCode' => array(
                '01',
            ),
            'display' => 'all',
        );

        $this->request
            ->method('getOptions')
            ->will($this->returnValue($options));
        $this->dbh
            ->method('queryall_array')
            ->will($this->returnValue(array()));
        // Make sure API_NOTFOUND is returned
        $this->sth
            ->method('fetchrow_assoc')
            ->will($this->onConsecutiveCalls($this->returnCallBack(array($this, 'mockFetchRow_B')), array()));

        $resp = $this->testScore->getTestScores();
        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }
}
