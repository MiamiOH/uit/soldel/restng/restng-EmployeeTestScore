<?php
namespace MiamiOH\RestngEmployeeTestScore\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EmployeeNewHireResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'EmployeeNewHire.Model',
            'type' => 'object',
            'properties' => array(
                'uniqueID' => array(
                    'type' => 'string',
                ),
                'pidm' => array(
                    'type' => 'string',
                ),
                'plusNumber' => array(
                    'type' => 'string',
                ),
                'emailAddress' => array(
                    'type' => 'string',
                ),
                'firstName' => array(
                    'type' => 'string',
                ),
                'lastName' => array(
                    'type' => 'string',
                ),
                'eclsCode' => array(
                    'type' => 'string',
                ),
                'egrpCode' => array(
                    'type' => 'string',
                ),
                'salaryGrade' => array(
                    'type' => 'string',
                ),
                'currentHireDate' => array(
                    'type' => 'string',
                ),

            )
        ));

        $this->addDefinition(array(
            'name' => 'EmployeeNewHire.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/EmployeeNewHire.Model'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'EmployeeNewHire',
            'class' => 'MiamiOH\RestngEmployeeTestScore\Services\EmployeeNewHire',
            'description' => 'Resource for retrieving and insert new hire employee\'s',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory')
            )
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Retrieves all new hire employee',
            'name' => 'employee.newHire.get.collection',
            'service' => 'EmployeeNewHire',
            'method' => 'getNewHires',
            'pattern' => '/employee/newHire/v1',
            'isPageable' => true,
            'defaultPageLimit' => 50,
            'maxPageLimit' => 500,
            'options' => array(
                'uniqueID' => array(
                    'type' => 'list',
                    'description' => 'Return only the employees that match uniqueID'
                ),
                'pidm' => array(
                    'type' => 'list',
                    'description' => 'Return only the employees that match pidm'
                ),
                'plusNumber' => array(
                    'type' => 'list',
                    'description' => 'Return only the employees that match plusNumber'
                ),
                'fromDate' => array(
                    'description' => 'Return only the employees that match fromDate. Date format:\'YYYY-MM-DD\''
                ),
                'toDate' => array(
                    'description' => 'Return only the employees that match toDate. Date format:\'YYYY-MM-DD\''
                ),

            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of Employee New Hire  records.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/EmployeeNewHire.Collection',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'There is no record found.',
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized access.',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}