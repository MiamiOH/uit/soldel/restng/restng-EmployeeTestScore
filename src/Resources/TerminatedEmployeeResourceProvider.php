<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 8/21/18
 * Time: 2:23 PM
 */

namespace MiamiOH\RestngEmployeeTestScore\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class TerminatedEmployeeResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'TerminatedEmployee.Model',
            'type' => 'object',
            'properties' => array(
                'uniqueID' => array(
                    'type' => 'string',
                ),
                'pidm' => array(
                    'type' => 'string',
                ),
                'plusNumber' => array(
                    'type' => 'string',
                ),
                'firstName' => array(
                    'type' => 'string',
                ),
                'lastName' => array(
                    'type' => 'string',
                ),
                'emailAddress' => array(
                    'type' => 'string',
                ),
                'employeeGroup' => array(
                    'type' => 'string',
                ),
                'terminationDate' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'TerminatedEmployee.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/TerminatedEmployee.Model'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'TerminatedEmployee',
            'class' => 'MiamiOH\RestngEmployeeTestScore\Services\TerminatedEmployee',
            'description' => 'Resource for retrieving terminated employee\'s',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory')
            )
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Retrieves all terminated employees',
            'name' => 'employee.terminatedEmployee.get.collection',
            'service' => 'TerminatedEmployee',
            'method' => 'getTerminatedEmployees',
            'pattern' => '/employee/terminatedEmployee/v1',
            'isPageable' => true,
            'isPartialable' => true,
            'defaultPageLimit' => 50,
            'maxPageLimit' => 500,
            'options' => array(
                'uniqueID' => array(
                    'type' => 'list',
                    'description' => 'Return only the employees that match uniqueID'
                ),
                'pidm' => array(
                    'type' => 'list',
                    'description' => 'Return only the employees that match pidm'
                ),
                'plusNumber' => array(
                    'type' => 'list',
                    'description' => 'Return only the employees that match plusNumber'
                ),
                'fromDate' => array(
                    'type' => 'single',
                    'description' => 'Find records after fromDate'
                ),
                'toDate' => array(
                    'type' => 'single',
                    'description' => 'Find records before toDate'
                ),

            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of test records.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/TerminatedEmployee.Collection',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'There is no record found.',
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized access.',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}