<?php

namespace MiamiOH\RestngEmployeeTestScore\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EmployeeTestScoreResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'EmployeeTestScore.Get.Model',
            'type' => 'object',
            'properties' => [
                'uniqueID' => [
                    'type' => 'string'
                ],
                'pidm' => array(
                    'type' => 'string',
                ),
                'bannerId' => array(
                    'type' => 'string',
                ),
                'examCode' => array(
                    'type' => 'string',
                ),
                'examDate' => array(
                    'type' => 'string',
                ),
                'examScore' => array(
                    'type' => 'integer',
                    'format' => 'int32',
                ),
                'examPercent' => array(
                    'type' => 'number',
                    'format' => 'double',
                ),
                'pass' => array(
                    'type' => 'string',
                ),
            ]
        ));

        $this->addDefinition(array(
            'name' => 'EmployeeTestScore.Put.Model',
            'type' => 'object',
            'properties' => array(
                'muid' => array(
                    'type' => 'string',
                ),
                'examCode' => array(
                    'type' => 'string',
                ),
                'examDate' => array(
                    'type' => 'string',
                ),
                'examScore' => array(
                    'type' => 'integer',
                    'format' => 'int32',
                ),
                'examPercent' => array(
                    'type' => 'number',
                    'format' => 'double',
                ),
                'pass' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'EmployeeTestScore.Delete.Model',
            'type' => 'object',
            'properties' => array(
                'muid' => array(
                    'type' => 'string',
                ),
                'examCode' => array(
                    'type' => 'string',
                ),
                'examDate' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'EmployeeTestScore.Get.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/EmployeeTestScore.Get.Model'
            )
        ));

        $this->addDefinition(array(
            'name' => 'EmployeeTestScore.Delete.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/EmployeeTestScore.Delete.Model'
            )
        ));

        $this->addDefinition(array(
            'name' => 'EmployeeTestScore.Put.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/EmployeeTestScore.Put.Model'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'EmployeeTestScore',
            'class' => 'MiamiOH\RestngEmployeeTestScore\Services\EmployeeTestScore\Service',
            'description' => 'Resource for retrieving and insert employee\'s test scores',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory')
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Retrieves all test scores of employee',
            'name' => 'employee.testScore.get.collection',
            'service' => 'EmployeeTestScore',
            'method' => 'getTestScores',
            'pattern' => '/employee/testScore/v2',
            'isPageable' => true,
            'isPartialable' => true,
            'defaultPageLimit' => 50,
            'maxPageLimit' => 500,
            'options' => array(
                'muid' => array(
                    'type' => 'list',
                    'description' => 'list of pidms, banner ids, unique ids'
                ),
                'examCode' => array(
                    'type' => 'list',
                    'description' => 'Return only the exams that match the given exam code'
                ),
                'pass' => array(
                    'description' => 'Return only the exams that match the given Pass(P)/Fail(F) value'
                ),
                'display' => array(
                    'enum' => array('latest', 'highest', 'lowest'),
                    'description' => 'A filter which returns a collection of latest, ' .
                        'highest score or lowest score (mutually exclusive) of current collection.'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of test records.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/EmployeeTestScore.Get.Collection',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'There is no record found.',
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized access.',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'description' => 'Creates/updates mutiple test scores for employees',
            'name' => 'employee.testScore.put.collection',
            'pattern' => '/employee/testScore/v2',
            'service' => 'EmployeeTestScore',
            'method' => 'putTestScores',
            'body' => array(
                'description' => 'Collection of Employee Test Score objects',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/EmployeeTestScore.Put.Collection',
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'Creates or updates successfully.',
                ),
                App::API_BADREQUEST => array(
                    'description' => 'Some or all data are bad.',
                ),
                App::API_FAILED => array(
                    'description' => 'Insert operation failed.',
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized access.',
                ),

            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));

        $this->addResource(array(
            'action' => 'delete',
            'description' => 'Delete mutiple test scores for employees',
            'name' => 'employee.testScore.delete.collection',
            'pattern' => '/employee/testScore/v2',
            'service' => 'EmployeeTestScore',
            'method' => 'deleteTestScores',
            'body' => array(
                'description' => 'Collection of Employee Test Score objects',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/EmployeeTestScore.Delete.Collection',
                )
            ),
            'responses' => array(
                App::API_ACCEPTED => array(
                    'description' => 'Delete successfully.',
                ),
                App::API_BADREQUEST => array(
                    'description' => 'Some or all data are bad.',
                ),
                App::API_FAILED => array(
                    'description' => 'Delete operation failed.',
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized access.',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}
