<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 9/5/18
 * Time: 11:00 AM
 */
namespace MiamiOH\RestngEmployeeTestScore\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EmployeeSupervisorResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'EmployeeSupervisor.Model',
            'type' => 'object',
            'properties' => array(
                'employeePIDM' => array(
                    'type' => 'string',
                ),
                'employeeEmailAddress' => array(
                    'type' => 'string',
                ),
                'supervisorPIDM' => array(
                    'type' => 'string',
                ),
                'supervisorEmailAddress' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'EmployeeSupervisor.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/EmployeeSupervisor.Model'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'EmployeeSupervisor',
            'class' => 'MiamiOH\RestngEmployeeTestScore\Services\EmployeeSupervisor',
            'description' => 'Resource for retrieving supervisor of an employee\'s',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory')
            )
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Retrieves supervisor of an employee',
            'name' => 'employee.supervisor.get.collection',
            'service' => 'EmployeeSupervisor',
            'method' => 'getSupervisor',
            'pattern' => '/employee/supervisor/v1',
            'isPageable' => true,
            'defaultPageLimit' => 50,
            'maxPageLimit' => 500,
            'options' => array(
                'uniqueID' => array(
                    'type' => 'list',
                    'description' => 'Return only the employees that match uniqueID'
                ),
                'pidm' => array(
                    'type' => 'list',
                    'description' => 'Return only the employees that match pidm'
                ),
                'plusNumber' => array(
                    'type' => 'list',
                    'description' => 'Return only the employees that match plusNumber'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of Employee Supervisor  records.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/EmployeeSupervisor.Collection',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'There is no record found.',
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized access.',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}