<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 8/21/18
 * Time: 2:24 PM
 */

namespace MiamiOH\RestngEmployeeTestScore\Services;

use MiamiOH\RestngEmployeeTestScore\Utilities\Authorization;
use MiamiOH\RestngEmployeeTestScore\Utilities\DateTime;
use MiamiOH\RestngEmployeeTestScore\Utilities\Validator;

class TerminatedEmployee extends \MiamiOH\RESTng\Service
{
    private $datasource = 'MUWS_GEN_PROD';

    private $dbh = null;

    /**
     * @var Validator
     */
    private $validator = null;

    /**
     * @var Authorization
     */
    private $authorization = null;

    /**
     * @var DateTime
     */
    private $dateTime = null;

    /**
     * @var \MiamiOH\RESTng\Util\Request
     */
    private $request = null;

    /**
     * @var \MiamiOH\RESTng\Util\Response
     */
    private $response = null;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource);
    }

    private function getDependencies()
    {
        $this->dateTime = new DateTime();
        $this->validator = new Validator();
        $this->authorization = new Authorization($this->getApiUser());

        $this->response = $this->getResponse();
        $this->request = $this->getRequest();
    }

    private $getRules = [
        'uniqueID' => ['regex:/^\w{1,8}$/'],
        'pidm' => ['regex:/^\d{1,8}$/'],
        'plusNumber'=> ['regex:/^\+[0-9]{1,8}$/'],
        'fromDate' => ['date'],
        'toDate' => ['date']
    ];

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function getTerminatedEmployees()
    {
        $this->getDependencies();

        $options = $this->request->getOptions();


        // VALIDATE OPTIONS
        $errors = [];

        $this->validator->setRules($this->getRules);

        if (!empty($options)) {
            $errors = $this->validator->validateOptions($options);
        }

        $partialFields = [];
        if ($this->request->isPartial()) {
            $partialFields = $this->request->getPartialRequestFields();
        }

        // Handle Validation Error
        if ($errors) {
            $this->response->setPayLoad(['errors' => $errors]);
            $this->response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $this->response;
        }

        $permList = array('read', 'view', 'all');
        if (!$this->authorization->isAuthByKeys($permList)) {
            $this->response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $this->response;
        }

        $minRow = $this->request->getOffset();
        $maxRow = $minRow + $this->request->getLimit() - 1;

        list($payLoad, $totalRecords) = $this->getCollection($options, $minRow, $maxRow, $partialFields);

        $this->response->setTotalObjects($totalRecords);
        $this->response->setPayLoad($payLoad);
        $this->response->setStatus(empty($payLoad) ? \MiamiOH\RESTng\App::API_NOTFOUND : \MiamiOH\RESTng\App::API_OK);

        return $this->response;
    }

    protected $fieldHash = array(
        'uniqueID' => 'szbuniq_unique_id',
        'pidm' => 'pebempl_pidm',
        'plusNumber' => 'spriden_id',
        'firstName' => 'spriden_first_name',
        'lastName' => 'spriden_last_name',
        'emailAddress' => 'goremal_email_address',
        'employeeGroup' => 'pebempl_egrp_code',
        'terminationDate' => 'pebempl_term_date',
    );

    /**
     * @param $options
     * @param $minRow
     * @param $maxRow
     * @param array $partialFields
     * @return array
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    protected function getCollection($options, $minRow, $maxRow, $partialFields = array())
    {
        $pidms = array();
        $idCount = 0;
        $whereConditions = array();
        $allParams = array();
        $records = array();
        $fields = $this->getPartialFields($partialFields);

        if (!empty($options['pidm'])) {
            $pidms = $options['pidm'];
            $idCount += count($options['pidm']);
        }
        if (!empty($options['uniqueID'])) {
            $pidms = array_merge($pidms, $this->getPidmsByUniqueIDs($options['uniqueID']));
            $idCount += count($options['uniqueID']);
        }

        if (!empty($options['plusNumber'])) {
            $pidms = array_merge($pidms, $this->getPidmsByPlusNumbers($options['plusNumber']));
            $idCount += count($options['plusNumber']);
        }

        if (!empty($options['fromDate'])) {
            $date = (new \DateTime($options['fromDate']))->format('m-d-Y');
            $whereConditions[] = "TRUNC(pebempl_term_date) >= TO_DATE(?, 'MM-DD-YYYY')";
            array_push($allParams, $date);
        }

        if (!empty($options['toDate'])) {
            $date = (new \DateTime($options['toDate']))->format('m-d-Y');
            $whereConditions[] = "TRUNC(pebempl_term_date) <= TO_DATE(?, 'MM-DD-YYYY')";
            array_push($allParams, $date);
        }

        if (!empty($pidms)) {
            $whereConditions[] = 'pebempl_pidm IN (?' . str_repeat(",?", count($pidms) - 1) . ')';
            $allParams = array_merge($allParams, $pidms);
        } elseif ($idCount > 0) {
            return array(array(), 0);
        }

        $whereClause = $whereConditions ? ' AND ' . implode(' AND ', $whereConditions) : '';

        $totalRecords = $this->dbh->queryfirstcolumn(
            $this->generateGetCollectionQuery('COUNT(*)', $whereClause),
            $allParams
        );

        $query = "SELECT $fields FROM (SELECT a.*, ROWNUM rnum FROM (" .
            $this->generateGetCollectionQuery(str_replace(
                'pebempl_term_date',
                "TO_CHAR(pebempl_term_date, 'yyyy-mm-dd\"T\"HH24:MI:SS') as pebempl_term_date",
                $fields
            ), $whereClause) .
            ") a WHERE rownum <= ?) WHERE rnum >= ?";

        array_push($allParams, $maxRow, $minRow);

        $queryHandler = $this->dbh->prepare($query);

        $queryHandler->execute($allParams);

        $fieldExchange = array_flip($this->fieldHash);

        while ($record = $queryHandler->fetchrow_assoc()) {
            foreach ($record as $key => $val) {
                $record[$fieldExchange[$key]] = $record[$key];
                unset($record[$key]);
            }
            $records[] = $record;
        }

        return array($records, $totalRecords);

    }

    /**
     * @param $fields
     * @param $whereClause
     * @return string
     */
    private function generateGetCollectionQuery($fields, $whereClause)
    {
        $query = "SELECT $fields FROM payroll.pebempl " .
            "LEFT JOIN saturn.spriden ON spriden_pidm = pebempl_pidm " .
            "LEFT JOIN saturn.szbuniq ON spriden_pidm = szbuniq_pidm " .
            "LEFT JOIN general.goremal ON pebempl_pidm = goremal_pidm AND goremal_emal_code = 'MU'" .
            "WHERE spriden_change_ind IS NULL$whereClause".
            " AND pebempl_empl_status = 'T' " .
            " AND pebempl_egrp_code IN ('ADMN', 'AFSC', 'AJCT', 'ERET', 'FAC', 'FOP', 'OTH', 'SATS', 'UP')";
        return $query;
    }

    private function getPidmsByUniqueIDs($uniqueIDs)
    {
        // precondition: count($uniqueID) > 0
        // if uniqueID is not empty, it mean validation must be passed, therefore exist at least 1 id
        $uniqueIDs = array_map(
            function ($val) {
                return strtoupper(trim($val));
            },
            $uniqueIDs
        );

        $records = $this->dbh->queryall_array(
            "SELECT DISTINCT szbuniq_pidm as pidm, szbuniq_unique_id as unique_id " .
            " FROM szbuniq WHERE szbuniq_unique_id IN (?" .
            str_repeat(",?", count($uniqueIDs) - 1) . ")",
            $uniqueIDs
        );

        $pidms = array_column($records, 'pidm');
        return $pidms;
    }

    /**
     * @param $partialFields
     * @return string
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     * @throws \Exception
     */
    private function getPartialFields($partialFields)
    {
        $ret = array();
        if (!empty($partialFields)) {
            foreach ($partialFields as $partialField) {
                if (!isset($this->fieldHash[$partialField])) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest($this->validator->getErrMesg('invalidField'));
                }
                $ret[] = $this->fieldHash[$partialField];
            }
        } else {
            $ret = array_values($this->fieldHash);
        }
        return implode(', ', $ret);
    }


    private function getPidmsByPlusNumbers($plusNumbers)
    {
        // precondition: count($plusNumbers) > 0
        $records = $this->dbh->queryall_array(
            "SELECT DISTINCT spriden_pidm as pidm FROM spriden WHERE spriden_id IN (?" .
            str_repeat(",?", count($plusNumbers) - 1) . ")",
            $plusNumbers
        );

        $pidms = array_column($records, 'pidm');
        return $pidms;
    }
}