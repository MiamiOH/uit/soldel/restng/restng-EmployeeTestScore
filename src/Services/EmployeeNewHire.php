<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 8/20/18
 * Time: 8:49 AM
 */

namespace MiamiOH\RestngEmployeeTestScore\Services;

use MiamiOH\RestngEmployeeTestScore\Utilities\Authorization;
use MiamiOH\RestngEmployeeTestScore\Utilities\DateTime;
use MiamiOH\RestngEmployeeTestScore\Utilities\Validator;

class EmployeeNewHire extends \MiamiOH\RESTng\Service
{
    private $datasource = 'MUWS_GEN_PROD';

    private $dbh = null;

    /**
     * @var Validator
     */
    private $validator = null;

    /**
     * @var Authorization
     */
    private $authorization = null;

    /**
     * @var DateTime
     */
    private $dateTime = null;

    /**
     * @var \MiamiOH\RESTng\Util\Request
     */
    private $request = null;

    /**
     * @var \MiamiOH\RESTng\Util\Response
     */
    private $response = null;

    private $getRules = [
        'uniqueID' => ['regex:/^\w{1,8}$/'],
        'pidm' => ['regex:/^\d{1,8}$/'],
        'plusNumber'=> ['regex:/^\+[0-9]{1,8}$/'],
        'fromDate' => ['date'],
        'toDate' => ['date']
    ];

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource);
    }

    private function getDependencies()
    {
        $this->dateTime = new DateTime();
        $this->validator = new Validator();
        $this->authorization = new Authorization($this->getApiUser());

        $this->response = $this->getResponse();
        $this->request = $this->getRequest();
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     * @throws \Exception
     */
    public function getNewHires()
    {
        $this->getDependencies();

        $options = $this->request->getOptions();

        $errors = [];

        $this->validator->setRules($this->getRules);

        if (!empty($options)) {
            $errors = $this->validator->validateOptions($options);
        }

        // Handle Validation Error
        if ($errors) {
            $this->response->setPayLoad(['errors' => $errors]);
            $this->response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $this->response;
        }

        $offset = $this->request->getOffset();
        $limit = $this->request->getLimit();

        $isPaged = $this->request->isPaged();

        $permList = array('read', 'view', 'all');
        if (!$this->authorization->isAuthByKeys($permList)) {
            $this->response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $this->response;
        }


        $whereClause = $isPaged ? ' where rnum <= ' . ($limit + $offset - 1) . ' and  rnum >= ' . $offset : '';

        $pidms = [];
        if (empty($options)) {
            $queryString = $this->buildQuery(') ' . $whereClause);
            $queryString = 'select * from (' . $queryString;

            $results = $this->dbh->queryall_array($queryString);
            $totalRecords = $this->getCountOfRecords($this->buildQuery(null));
        }

        if (array_key_exists('uniqueID', $options)) {
            $pidms = array_merge($pidms, $this->getPidmsByUniqueIDs($options['uniqueID']));
        }

        if (array_key_exists('plusNumber', $options)) {
            $pidms = array_merge($pidms, $this->getPidmsByPlusNumbers($options['plusNumber']));
        }

        array_key_exists('pidm', $options) ? $pidms = array_merge($pidms, $options['pidm']) : '';

        if (count($pidms) > 0 && isset($options['fromDate'])) {
            $pidms = array_map('trim', $pidms);
            $pidms = "'" . implode("','", $pidms) . "'";

            $pidms = preg_replace('/\s+/', '', $pidms);
            $queryString = $this->buildQuery("and szbuniq_pidm in ($pidms)");

            list($totalRecords, $results) = $this->getDataByDates($options, $queryString, $whereClause);
        } else if (count($pidms) > 0) {
            $pidms = array_map('trim', $pidms);
            $pidms = "'" . implode("','", $pidms) . "'";
            $pidms = preg_replace('/\s+/', '', $pidms);

            $queryString = $this->buildQuery("and szbuniq_pidm in ($pidms)");
            $totalRecords = count($this->dbh->queryall_array($queryString));

            $queryString .= ') ' . $whereClause;

            $queryString = 'select * from (' . $queryString;

            $results = $this->dbh->queryall_array($queryString);
        } else if (isset($options['fromDate'])) {
            $queryString = $this->buildQuery(null);
            list($totalRecords, $results) = $this->getDataByDates($options, $queryString, $whereClause);
        }

        if (empty($results)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('No record found.');
        }

        $formattedEmployeeHires = [];
        foreach ($results as $result) {
            $employeeHires['uniqueId'] = $result['szbuniq_unique_id'];
            $employeeHires['pidm'] = $result['szbuniq_pidm'];
            $employeeHires['emailAddress'] = $result['email_address'];
            $employeeHires['plusNumber'] = $result['spriden_id'];
            $employeeHires['lastName'] = $result['spriden_last_name'];
            $employeeHires['firstName'] = $result['spriden_first_name'];
            $employeeHires['eclsCode'] = $result['pebempl_ecls_code'];
            $employeeHires['egrpCode'] = $result['pebempl_egrp_code'];
            $employeeHires['salaryGrade'] = $result['nbrjobs_sal_grade'];
            $employeeHires['currentHireDate'] = date(DATE_ATOM, strtotime($result['pebempl_current_hire_date']));

            $formattedEmployeeHires[] = $employeeHires;
        }

        $this->response->setTotalObjects($totalRecords);
        $this->response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $this->response->setPayload($formattedEmployeeHires);


        return $this->response;
    }

    /**
     * @return string
     */
    private function buildQuery($whereClause)
    {
        $query = "select  
            distinct
            szbuniq_unique_id,
            szbuniq_pidm,
            lower(szbuniq_unique_id)||'@miamioh.edu' email_address,
            spriden_id,
            spriden_last_name,
            spriden_first_name,
            pebempl_ecls_code,
            pebempl_egrp_code,
            a1.nbrjobs_sal_grade,
            pebempl_current_hire_date,
            ROWNUM rnum
            from
            spriden,
            szbuniq,
            pebempl,
            nbrjobs a1,
            nbrbjob a
            where spriden_Pidm = pebempl_Pidm
            and spriden_pidm = szbuniq_pidm
            and spriden_change_ind is null
            and pebempl_empl_status = 'A'
            and pebempl_pidm= a1.nbrjobs_pidm
            and pebempl_egrp_code in ('ADMN', 'AFSC', 'AJCT', 'ERET', 'FAC', 'FOP', 'OTH', 'SATS', 'UP')
            and a.NBRBJOB_PIDM = a1.NBRJOBS_PIDM
                                    and a.NBRBJOB_POSN = NBRJOBS_POSN
                                    and a.NBRBJOB_SUFF = NBRJOBS_SUFF
                                    and NBRJOBS_STATUS <> 'T'
                                    and a.nbrbjob_contract_type in ('P')
                                    and a1.nbrjobs_effective_date = (select max(b.nbrjobs_effective_date)
                                                                          from nbrjobs b
                                                                          where b.nbrjobs_pidm = a1.nbrjobs_pidm
                                                                          and b.NBRJOBS_POSN = a1.NBRJOBS_POSN
                                                                          and b.NBRJOBS_SUFF = a1.NBRJOBS_SUFF
                                                                          and b.nbrjobs_effective_date <= sysdate)
                                                                          $whereClause";

        return $query;
    }

    /**
     * @return string
     */
    private function getCountOfRecords($queryString)
    {
        $queryStringForTotalRecords = 'select count(*) from (' . $queryString . ')';

        return $this->dbh->queryfirstcolumn($queryStringForTotalRecords);

    }

    private function getPidmsByUniqueIDs($uniqueIDs)
    {
        $uniqueIDs = array_map(
            function ($val) {
                return strtoupper(trim($val));
            },
            $uniqueIDs
        );

        $records = $this->dbh->queryall_array(
            "SELECT DISTINCT szbuniq_pidm as pidm, szbuniq_unique_id as unique_id " .
            " FROM szbuniq WHERE szbuniq_unique_id IN (?" .
            str_repeat(",?", count($uniqueIDs) - 1) . ")",
            $uniqueIDs
        );

        $pidms = array_column($records, 'pidm');
        return $pidms;
    }

    private function getPidmsByPlusNumbers($plusNumbers)
    {
        $records = $this->dbh->queryall_array(
            "SELECT DISTINCT spriden_pidm as pidm FROM spriden WHERE spriden_id IN (?" .
            str_repeat(",?", count($plusNumbers) - 1) . ")",
            $plusNumbers
        );

        $pidms = array_column($records, 'pidm');
        return $pidms;
    }

    /**
     * @param $options
     * @param $queryString
     * @param $whereClause
     * @return array
     */
    private function getDataByDates($options, $queryString, $whereClause)
    {
        $fromDate = (new \DateTime($options['fromDate']))
            ->setTimezone(new \DateTimeZone('US/Eastern'))
            ->format('m-d-Y');

        if (isset($options['toDate'])) {
            $toDate = (new \DateTime($options['toDate']))
                ->setTimezone(new \DateTimeZone('US/Eastern'))
                ->format('m-d-Y');

            $queryString .= " AND TRUNC(pebempl_current_hire_date)
                            BETWEEN TO_DATE(?, 'MM-DD-YYYY') 
                             AND TO_DATE(?, 'MM-DD-YYYY')
                             ";
            $totalRecords = count($this->dbh->queryall_array($queryString, $fromDate, $toDate));

            $queryString .= ') ' . $whereClause;
            $queryString = 'select * from (' . $queryString;

            $results = $this->dbh->queryall_array($queryString, $fromDate, $toDate);

        } else {

            $queryString .= " AND TRUNC(pebempl_current_hire_date)
                            BETWEEN TO_DATE(?, 'MM-DD-YYYY') 
                            and sysdate
                            ";


            $totalRecords = count($this->dbh->queryall_array($queryString, $fromDate));

            $queryString .= ') ' . $whereClause;
            $queryString = 'select * from (' . $queryString;
            $results = $this->dbh->queryall_array($queryString, $fromDate);
        }
        return array($totalRecords, $results);
    }


}