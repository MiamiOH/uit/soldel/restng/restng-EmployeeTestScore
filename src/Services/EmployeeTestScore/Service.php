<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 8/26/18
 * Time: 7:14 PM
 */

namespace MiamiOH\RestngEmployeeTestScore\Services\EmployeeTestScore;

use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RestngEmployeeTestScore\DataAccesses\EmployeeTestScoreDA;
use MiamiOH\RestngEmployeeTestScore\Utilities\Authorization;
use MiamiOH\RestngEmployeeTestScore\Utilities\DateTime;
use MiamiOH\RestngEmployeeTestScore\Utilities\Validator;

class Service extends \MiamiOH\RESTng\Service
{
    protected $datasource = 'MUWS_GEN_PROD';

    protected $dbh = null;

    /**
     * @var Validator
     */
    private $validator = null;

    /**
     * @var Authorization
     */
    private $authorization = null;

    /**
     * @var DateTime
     */
    private $dateTime = null;

    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * @var EmployeeTestScoreDA
     */
    protected $dataAccess = null;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource);
    }

    public function getDependencies()
    {
        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $this->dateTime = new DateTime();
        $this->validator = new Validator();
        $this->authorization = new Authorization($this->user);
        $this->dataAccess = new EmployeeTestScoreDA($this->dbh);
    }

    /**
     * @throws \Exception
     */
    public function deleteTestScores()
    {

        $this->getDependencies();

        $deleteService = new Delete();

        $response = $deleteService->deleteTestScores(
            $this->request,
            $this->response,
            $this->dataAccess,
            $this->authorization,
            $this->validator,
            $this->dateTime
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function putTestScores()
    {

        $this->getDependencies();

        $putService = new Put();

        $response = $putService->putTestScores(
            $this->request,
            $this->response,
            $this->dataAccess,
            $this->authorization,
            $this->validator,
            $this->dateTime,
            $this->user
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function getTestScores()
    {

        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getTestScores(
            $this->request,
            $this->response,
            $this->dataAccess,
            $this->authorization,
            $this->validator
        );

        return $response;
    }


}