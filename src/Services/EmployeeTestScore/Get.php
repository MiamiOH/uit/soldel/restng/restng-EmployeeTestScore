<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 8/26/18
 * Time: 1:32 AM
 */

namespace MiamiOH\RestngEmployeeTestScore\Services\EmployeeTestScore;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RestngEmployeeTestScore\DataAccesses\EmployeeTestScoreDA;
use MiamiOH\RestngEmployeeTestScore\Utilities\Authorization;
use MiamiOH\RestngEmployeeTestScore\Utilities\DateTime;
use MiamiOH\RestngEmployeeTestScore\Utilities\Validator;

class Get
{
    private $getRules = [
        'muid' => ['regex:/^(\w{1,8}|\d{1,8}|\+[0-9]{1,8})$/'],
        'examCode' => ['max:2'],
        'pass' => ['max:1'],
        'display' => ['in:highest,lowest,latest'],
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param EmployeeTestScoreDA $dataAccess
     * @param Authorization $authorization
     * @param Validator $validator
     * @param DateTime $dateTime
     * @return Response
     * @throws \Exception
     */
    public function getTestScores(
        Request $request,
        Response $response,
        EmployeeTestScoreDA $dataAccess,
        Authorization $authorization,
        Validator $validator
    ): Response
    {
        $permList = array('read', 'view', 'all');

        if (!$authorization->isAuthByKeys($permList)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }

        $partialFields = [];

        if ($request->isPartial()) {
            $partialFields = $request->getPartialRequestFields();
        }

        $options = $request->getOptions();

        $validator->setRules($this->getRules);

        $errors = [];

        if (!empty($options)) {
            $errors = $validator->validateOptions($options);
        }

        // Handle Validation Error
        if ($errors) {
            $response->setPayLoad(['errors' => $errors]);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $response;
        }

        $minRow = $request->getOffset();
        $maxRow = $minRow + $request->getLimit() - 1;

        list($payLoad, $totalRecords) = $dataAccess->getEmployeeTestScores(
            $options['muid'] ?? [],
            $options['examCode'] ?? [],
            $options['pass'] ?? '',
            $options['display'] ?? '',
            $minRow,
            $maxRow,
            $partialFields
        );

        $response->setTotalObjects($totalRecords);
        $response->setPayLoad($payLoad);
        $response->setStatus($totalRecords === 0 ? \MiamiOH\RESTng\App::API_NOTFOUND : \MiamiOH\RESTng\App::API_OK);

        return $response;
    }
}