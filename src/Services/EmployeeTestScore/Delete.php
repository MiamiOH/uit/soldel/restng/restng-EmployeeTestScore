<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 8/26/18
 * Time: 1:32 AM
 */

namespace MiamiOH\RestngEmployeeTestScore\Services\EmployeeTestScore;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RestngEmployeeTestScore\DataAccesses\EmployeeTestScoreDA;
use MiamiOH\RestngEmployeeTestScore\Utilities\Authorization;
use MiamiOH\RestngEmployeeTestScore\Utilities\DateTime;
use MiamiOH\RestngEmployeeTestScore\Utilities\Validator;

class Delete
{
    private $deleteRules = [
        'muid' => ['regex:/^(\w{1,8}|\d{1,8}|\+[0-9]{1,8})$/', 'required'],
        'examDate' => ['date', 'required'],
        'examCode' => ['max:2', 'required']
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param EmployeeTestScoreDA $dataAccess
     * @param Authorization $authorization
     * @param Validator $validator
     * @param DateTime $dateTime
     * @return mixed
     * @throws \Exception
     */
    public function deleteTestScores(
        Request $request,
        Response $response,
        EmployeeTestScoreDA $dataAccess,
        Authorization $authorization,
        Validator $validator,
        DateTime $dateTime
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_ACCEPTED;
        $payLoad = [
            'successes' => [],
            'errors' => []
        ];

        $permList = array('delete', 'all');
        if (!$authorization->isAuthByKeys($permList)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }

        // GET DATA
        $data = $request->getData();

        if (empty($data)) {
            $payLoad['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayLoad($payLoad);
            $response->setStatus($status);
            return $response;
        }

        // SET VALIDATION RULES
        $validator->setRules($this->deleteRules);

        $successCount = 0;

        // VALIDATE EACH ROW
        foreach ($data as $row) {
            $errors = $validator->validate($row);

            // Handle Validation Error
            if ($errors) {
                $status = \MiamiOH\RESTng\App::API_BADREQUEST;

                $row['error_messages'] = $errors;
                $payLoad['errors'][] = $row;
                continue;
            }

            // FORMAT EXAM DATE
            $row['examDate'] = $dateTime->formatStandard($row['examDate']);

            $dataResponse = $dataAccess->deleteSingle(
                $row['muid'],
                $row['examCode'],
                $row['examDate']
            );

            // Handle Data Operation Error
            if ($dataResponse->hasError()) {
                $status = \MiamiOH\RESTng\App::API_BADREQUEST;

                $row['error_messages'] = $dataResponse->getErrors();
                $payLoad['errors'][] = $row;
            }
            $successCount++;
        }

        $payLoad['successes'][] = "$successCount record(s) deleted.";

        // RETURN
        $response->setPayLoad($payLoad);
        $response->setStatus($status);
        return $response;
    }
}