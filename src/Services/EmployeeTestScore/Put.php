<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 8/26/18
 * Time: 1:32 AM
 */

namespace MiamiOH\RestngEmployeeTestScore\Services\EmployeeTestScore;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RestngEmployeeTestScore\DataAccesses\EmployeeTestScoreDA;
use MiamiOH\RestngEmployeeTestScore\Utilities\Authorization;
use MiamiOH\RestngEmployeeTestScore\Utilities\DateTime;
use MiamiOH\RestngEmployeeTestScore\Utilities\Validator;

class Put
{
    private $putRules = [
        'muid' => ['regex:/^(\w{1,8}|\d{1,8}|\+[0-9]{1,8})$/', 'required'],
        'examDate' => ['date', 'required'],
        'examCode' => ['max:2', 'required'],
        'examScore' => ['numeric', 'regex:/^[+-]?\d{1,4}(\.\d*)?$/'],
        'examPercent' => ['numeric', 'regex:/^[+-]?\d{1,3}(\.\d*)?$/'],
        'pass' => ['max:1']
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param EmployeeTestScoreDA $dataAccess
     * @param Authorization $authorization
     * @param Validator $validator
     * @param DateTime $dateTime
     * @param User $user
     * @return Response
     * @throws \Exception
     */
    public function putTestScores(
        Request $request,
        Response $response,
        EmployeeTestScoreDA $dataAccess,
        Authorization $authorization,
        Validator $validator,
        DateTime $dateTime,
        User $user
    ): Response
    {
        $payLoad = [
            'successes' => [],
            'errors' => []
        ];

        $status = \MiamiOH\RESTng\App::API_CREATED;

        $permList = array('create', 'update', 'edit', 'all');

        if (!$authorization->isAuthByKeys($permList)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }

        # Make sure data is there
        $data = $request->getData();
        if (empty($data)) {
            $payLoad['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayLoad($payLoad);
            $response->setStatus($status);
            return $response;
        }

        $successCount = 0;

        // SET VALIDATION RULES
        $validator->setRules($this->putRules);

        foreach ($data as $row) {
            $errors = $validator->validate($row);

            // Handle Validation Error
            if ($errors) {
                $status = \MiamiOH\RESTng\App::API_BADREQUEST;

                $row['error_messages'] = $errors;
                $payLoad['errors'][] = $row;
                continue;
            }

            // FORMAT EXAM DATE
            $row['examDate'] = $dateTime->formatStandard($row['examDate']);
            $dataResponse = $dataAccess->putSingle(
                $row['muid'],
                $row['examCode'],
                $row['examDate'],
                $row['pass'] ?? '',
                $row['examScore'] ?? '',
                $row['examPercent'] ?? '',
                $user->getUsername(),
                'WebService'
            );

            // Handle Data Operation Error
            if ($dataResponse->hasError()) {
                $status = \MiamiOH\RESTng\App::API_BADREQUEST;
                $row['error_messages'] = $dataResponse->getErrors();
                $payLoad['errors'][] = $row;
            } else {
                $successCount++;
            }
        }

        $payLoad['successes'][] = "$successCount record(s) created or updated.";

        // DONE
        $response->setPayLoad($payLoad);
        $response->setStatus($status);
        return $response;
    }

}