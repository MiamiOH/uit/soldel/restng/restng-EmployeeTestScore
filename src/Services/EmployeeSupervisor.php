<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 9/5/18
 * Time: 11:21 AM
 */

namespace MiamiOH\RestngEmployeeTestScore\Services;

use MiamiOH\RestngEmployeeTestScore\Utilities\Authorization;
use MiamiOH\RestngEmployeeTestScore\Utilities\Validator;

class EmployeeSupervisor extends \MiamiOH\RESTng\Service
{
    private $datasource = 'MUWS_GEN_PROD';

    private $dbh = null;

    /**
     * @var Validator
     */
    private $validator = null;

    /**
     * @var Authorization
     */
    private $authorization = null;

    /**
     * @var \MiamiOH\RESTng\Util\Request
     */
    private $request = null;

    /**
     * @var \MiamiOH\RESTng\Util\Response
     */
    private $response = null;

    private $getRules = [
        'uniqueID' => ['regex:/^\w{1,8}$/'],
        'pidm' => ['regex:/^\d{1,8}$/'],
        'plusNumber' => ['regex:/^\+[0-9]{1,8}$/'],
    ];

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource);
    }

    private function getDependencies()
    {
        $this->validator = new Validator();
        $this->authorization = new Authorization($this->getApiUser());

        $this->response = $this->getResponse();
        $this->request = $this->getRequest();
    }

    public function getSupervisor()
    {
        $this->getDependencies();

        $options = $this->request->getOptions();

        $errors = [];

        $this->validator->setRules($this->getRules);

        if (!empty($options)) {
            $errors = $this->validator->validateOptions($options);
        }

        // Handle Validation Error
        if ($errors) {
            $this->response->setPayLoad(['errors' => $errors]);
            $this->response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $this->response;
        }

        $offset = $this->request->getOffset();
        $limit = $this->request->getLimit();

        $isPaged = $this->request->isPaged();

        $permList = array('read', 'view', 'all');
        if (!$this->authorization->isAuthByKeys($permList)) {
            $this->response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $this->response;
        }

        $whereClause = $isPaged ? ' where rnum <= ' . ($limit + $offset - 1) . ' and  rnum >= ' . $offset : '';

        $pidms = [];

        $results =[];

        $totalRecords = 0;

        if (array_key_exists('uniqueID', $options)) {
            $pidms = array_merge($pidms, $this->getPidmsByUniqueIDs($options['uniqueID']));
        }

        if (array_key_exists('plusNumber', $options)) {
            $pidms = array_merge($pidms, $this->getPidmsByPlusNumbers($options['plusNumber']));
        }

        array_key_exists('pidm', $options) ? $pidms = array_merge($pidms, $options['pidm']) : '';


        if (count($pidms) > 0) {
            $pidms = array_map('trim', $pidms);
            $pidms = "'" . implode("','", $pidms) . "'";
            $pidms = preg_replace('/\s+/', '', $pidms);

            $queryString = $this->buildQuery("AND a.nbrjobs_pidm IN ($pidms)ORDER BY a.nbrjobs_effective_date DESC");
            $totalRecords = count($this->dbh->queryall_array($queryString));
            $queryString .= ') ' . $whereClause;

            $queryString = 'select * from (' . $queryString;

            $results = $this->dbh->queryall_array($queryString);
        }

        $formattedEmployeeHires = [];
        foreach ($results as $result) {
            $employeeHires['employerPIDM'] = $result['nbrjobs_pidm'];

            $employeeEmail = null;
            if ($employeeHires['employerPIDM'] !== null) {
                $employeeResponse = $this->callResource('person.email.get',
                    array(
                        'options' => array('pidm' => $employeeHires['employerPIDM'])
                    ));

                if ($employeeResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
                    $this->response->setPayload(array('message' => 'User not found when retrieving user email id'));
                    $this->response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
                    return $this->response;
                }
                $employees[] = $employeeResponse->getPayload();

                foreach ($employees as $employee) {
                    foreach ($employee as $email) {
                        if ($email['emailCode'] === 'MU') {
                            $employeeEmail = $email['emailAddress'];
                        }

                    }
                }
            }

            $employeeHires['employeeEmailAddress'] = $employeeEmail;

            $employeeHires['supervisorPIDM'] = $result['nbrjobs_supervisor_pidm'];

            $supervisorEmail = null;
            if ($employeeHires['supervisorPIDM'] !== null) {
                $supervisorResponse = $this->callResource('person.email.get',
                    array(
                        'options' => array('pidm' => $employeeHires['supervisorPIDM'])
                    ));

                if ($supervisorResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
                    $this->response->setPayload(array('message' => 'User not found when retrieving user email id'));
                    $this->response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
                    return $this->response;
                }
                $supervisors[] = $supervisorResponse->getPayload();

                foreach ($supervisors as $supervisor) {
                    foreach ($supervisor as $email) {
                        if ($email['emailCode'] === 'MU') {
                            $supervisorEmail = $email['emailAddress'];
                        }

                    }
                }
            }

            $employeeHires['supervisorEmailAddress'] = $supervisorEmail;

            $formattedEmployeeHires[] = $employeeHires;
        }


        $this->response->setTotalObjects($totalRecords);
        $this->response->setStatus($totalRecords === 0 ? \MiamiOH\RESTng\App::API_NOTFOUND : \MiamiOH\RESTng\App::API_OK);
        $this->response->setPayload($formattedEmployeeHires);


        return $this->response;
    }

    public function buildQuery($whereClause)
    {
        $query = "select 
                      nbrjobs_pidm,
                      nbrjobs_supervisor_pidm,
                      ROWNUM rnum
                      from
                      nbrjobs a
                      inner join nbrbjob on a.nbrjobs_pidm = nbrbjob.nbrbjob_pidm
                      and a.nbrjobs_posn = nbrbjob.nbrbjob_posn
                      and a.nbrjobs_suff = nbrbjob.nbrbjob_suff
                      where a.nbrjobs_status <> 'T'
                      and nbrbjob_contract_type = 'P'
                      and (nbrbjob_end_date >= SYSDATE
                      or nbrbjob_end_date is null)
                      and a.nbrjobs_effective_date = (select max(b.nbrjobs_effective_date)
                                                          from nbrjobs b
                                                           where b.nbrjobs_pidm = a.nbrjobs_pidm
                                                           and b.nbrjobs_posn = a.nbrjobs_posn
                                                           and b.nbrjobs_suff = a.nbrjobs_suff
                                                           and b.nbrjobs_status <> 'T')
                                                           $whereClause";

        return $query;
    }

    private function getPidmsByUniqueIDs($uniqueIDs)
    {
        $uniqueIDs = array_map(
            function ($val) {
                return strtoupper(trim($val));
            },
            $uniqueIDs
        );

        $records = $this->dbh->queryall_array(
            "SELECT DISTINCT szbuniq_pidm as pidm, szbuniq_unique_id as unique_id " .
            " FROM szbuniq WHERE szbuniq_unique_id IN (?" .
            str_repeat(",?", count($uniqueIDs) - 1) . ")",
            $uniqueIDs
        );

        $pidms = array_column($records, 'pidm');
        return $pidms;
    }

    private function getPidmsByPlusNumbers($plusNumbers)
    {
        $records = $this->dbh->queryall_array(
            "SELECT DISTINCT spriden_pidm as pidm FROM spriden WHERE spriden_id IN (?" .
            str_repeat(",?", count($plusNumbers) - 1) . ")",
            $plusNumbers
        );

        $pidms = array_column($records, 'pidm');
        return $pidms;
    }
}