<?php

namespace MiamiOH\RestngEmployeeTestScore\DataAccesses;

use MiamiOH\RestngEmployeeTestScore\Utilities\DataResponse;

/**
 * Class EmployeeTestScoreDA
 * @package MiamiOH\RestngEmployeeTestScore\DataAccesses
 */
class EmployeeTestScoreDA
{
    /**
     * @var null
     */
    private $dbh = null;

    /**
     * EmployeeTestScoreDA constructor.
     * @param $databaseHandler
     */
    public function __construct($databaseHandler)
    {
        $this->dbh = $databaseHandler;
    }

    /**
     * @var array
     */
    protected $fieldHash = array(
        'uniqueID' => 'szbuniq_unique_id',
        'pidm' => 'pprexam_pidm',
        'plusNumber' => 'spriden_id',
        'examCode' => 'pprexam_exam_code',
        'examDescription' => 'ptrexam_desc',
        'examDate' => 'pprexam_exam_date',
        'examScore' => 'pprexam_exam_score',
        'examPercent' => 'pprexam_exam_percent',
        'pass' => 'pprexam_pass_ind',
    );

    /*
      @return array of payload and number of total records
    */
    /**
     * @param array $muids
     * @param array $examCode
     * @param string $pass
     * @param string $display
     * @param int $minRow
     * @param int $maxRow
     * @param array $partialFields
     * @return array
     * @throws \Exception
     */
    public function getEmployeeTestScores(
        array $muids,
        array $examCode,
        string $pass,
        string $display,
        int $minRow,
        int $maxRow,
        array $partialFields = []
    )
    {
        $whereConditions = [];
        $allParams = [];
        $records = [];
        $fields = $this->getPartialFields($partialFields);

        if (!empty($muids)) {
            $pidms = $this->getPidms($muids);

            if (count($pidms) === 0) {
                return [[], 0];
            } else {
                $whereConditions[] = 'pprexam_pidm IN (' . rtrim(str_repeat("?,", count($pidms)), ',') . ')';
                $allParams = array_merge($allParams, $pidms);
            }
        }

        if (!empty($examCode)) {
            $whereConditions[] = 'pprexam_exam_code IN ('
                . rtrim(str_repeat("?,", count($examCode)), ',') . ')';
            $allParams = array_merge($allParams, $examCode);
        }

        if (!empty($pass)) {
            $whereConditions[] = 'pprexam_pass_ind IN (?)';
            array_push($allParams, strtoupper($pass));
        }

        $whereStatement = $whereConditions ? ' AND ' . implode(' AND ', $whereConditions) : '';

        if (!empty($display) && $display !== 'all') {
            $whereStatement .= ' AND ' . $this->getDisplayQuery($display, $whereStatement);

            $allParams = array_merge($allParams, $allParams); //Duplicate params because $whereStatement repeats twice
        }

        $totalRecords = $this->dbh->queryfirstcolumn(
            $this->generateGetQuery('COUNT(*)', $whereStatement),
            $allParams
        );

        $query = "SELECT " .
            str_replace(
                'pprexam_exam_date',
                "TO_CHAR(pprexam_exam_date, 'yyyy-mm-dd\"T\"HH24:MI:SS') as pprexam_exam_date",
                $fields
            ) .
            " FROM (SELECT a.*, ROWNUM rnum FROM(" .
            $this->generateGetQuery($fields, $whereStatement) .
            ") a WHERE rownum <= ?) WHERE rnum >= ?";

        array_push($allParams, $maxRow, $minRow);

        $queryHandler = $this->dbh->prepare($query);

        $queryHandler->execute($allParams);

        $fieldExchange = array_flip($this->fieldHash);

        while ($record = $queryHandler->fetchrow_assoc()) {
            foreach ($record as $key => $val) {
                $record[$fieldExchange[$key]] = $record[$key];
                unset($record[$key]);
            }
            $records[] = $record;
        }

        return [$records, $totalRecords];
    }

    /**
     * @param $key
     * @param $whereStatement
     * @return mixed
     */
    private function getDisplayQuery($key, $whereStatement)
    {
        $displayHash = [
            'highest' => "pprexam_exam_score = " .
                "(SELECT MAX(pprexam_exam_score) FROM payroll . pprexam WHERE 1 = 1$whereStatement)",

            'lowest' => "pprexam_exam_score = " .
                "(SELECT MIN(pprexam_exam_score) FROM payroll . pprexam WHERE 1 = 1$whereStatement)",

            'latest' => "pprexam_exam_date = " .
                "(SELECT MAX(pprexam_exam_date) FROM payroll . pprexam WHERE 1 = 1$whereStatement)",
        ];
        return $displayHash[$key];
    }

    /**
     * @param $partialFields
     * @return string
     * @throws \Exception
     */
    private function getPartialFields($partialFields)
    {
        $ret = [];

        if (!empty($partialFields)) {
            foreach ($partialFields as $partialField) {
                if (!isset($this->fieldHash[$partialField])) {
                    throw new \Exception("Partial field $partialField is not found.");
                }
                $ret[] = $this->fieldHash[$partialField];
            }
        } else {
            $ret = array_values($this->fieldHash);
        }
        return implode(', ', $ret);
    }

    /**
     * @param $fields
     * @param $whereStatement
     * @return string
     */
    private function generateGetQuery($fields, $whereStatement)
    {
        $query = "SELECT $fields FROM payroll . pprexam " .
            "LEFT JOIN payroll . ptrexam ON pprexam_exam_code = ptrexam_code " .
            "LEFT JOIN saturn . spriden ON pprexam_pidm = spriden_pidm " .
            "LEFT JOIN saturn . szbuniq ON pprexam_pidm = szbuniq_pidm " .
            "WHERE spriden_change_ind IS NULL$whereStatement";
        return $query;
    }

    /**
     * @param string $muid
     * @param string $examCode
     * @param string $examDate
     * @param string $pass
     * @param string $examScore
     * @param string $examPercent
     * @param string $userId
     * @param string $dataOrigin
     * @return DataResponse
     */
    public function putSingle(
        string $muid,
        string $examCode,
        string $examDate,
        string $pass,
        string $examScore,
        string $examPercent,
        string $userId,
        string $dataOrigin
    )
    {
        $dataResponse = new DataResponse();

        try {
            $pidm = $this->getPidm($muid);

            if ($pidm === DB_EMPTY_SET) {
                throw new \Exception('Provided MUID does not match any pidm.');
            }

            if(floatval($examScore) < 0 ||
                floatval($examScore) > 100
            ) {
                throw new \Exception('Exam score must between 0 to 100.');
            }

            if(floatval($examPercent) < 0 ||
                floatval($examPercent) > 100
            ) {
                throw new \Exception('Exam percent must between 0 to 100.');
            }

            if($pass != 'P' && $pass != 'F')
            {
                throw new \Exception('Pass code must be P or F.');
            }

            $sql = "select ptrexam_code from ptrexam";
            $queryHandler = $this->dbh->prepare($sql);
            $queryHandler->execute();

            $examCodes = array();
            while ($record = $queryHandler->fetchrow_assoc()) {
                foreach ($record as $key => $val) {
                    array_push($examCodes, $record[$key]);
                }
            }
            if (!in_array($examCode, $examCodes)) {
                throw new \Exception('Exam code is not valid.');
            }

            $dateSQL = "TO_DATE(:examDate, 'yyyy-mm-dd\"T\"HH24:MI:SS')";

            $sql = "MERGE INTO payroll . pprexam USING DUAL ON " .
                "(pprexam_pidm = $pidm AND pprexam_exam_code = :examCode AND pprexam_exam_date = $dateSQL) " .
                "WHEN MATCHED THEN UPDATE SET " .
                "pprexam_exam_score = :examScore, pprexam_exam_percent = :examPercent, pprexam_pass_ind = :pass, " .
                "pprexam_user_id = :userID, pprexam_data_origin = :dataOrigin " .
                "WHEN NOT MATCHED THEN INSERT " .
                "(pprexam_pidm, pprexam_exam_code, pprexam_exam_date, pprexam_exam_score, " .
                "pprexam_exam_percent, pprexam_activity_date, pprexam_pass_ind, pprexam_user_id, pprexam_data_origin)" .
                "VALUES($pidm, :examCode, $dateSQL, :examScore, :examPercent, SYSDATE, :pass, :userID, :dataOrigin)";

            $sth = $this->dbh->prepare($sql);

            $pass = strtoupper($pass);

            $sth->bind_by_name('examCode', $examCode);
            $sth->bind_by_name('examDate', $examDate);
            $sth->bind_by_name('examScore', $examScore);
            $sth->bind_by_name('examPercent', $examPercent);
            $sth->bind_by_name('pass', $pass);
            $sth->bind_by_name('userID', $userId);
            $sth->bind_by_name('dataOrigin', $dataOrigin);

            $sth->execute();
        } catch (\Exception $e) {
            $dataResponse->addError($e->getMessage() . '. ' . $e->getTraceAsString());
        }

        return $dataResponse;
    }

    /**
     * @param string $muid
     * @return string
     */
    public function getPidm(string $muid): string
    {
        $uniqueIdRegex = '/^\w{1,8}$/';
        $pidmRegex = '/^\d{1,8}$/';
        $bannerIdRegex = '/^\+[0-9]{1,8}$/';

        if (preg_match($pidmRegex, $muid)) {
            $field = 'szbuniq_pidm';
        } elseif (preg_match($bannerIdRegex, $muid)) {
            $field = 'szbuniq_banner_id';
        } elseif (preg_match($uniqueIdRegex, $muid)) {
            $field = 'szbuniq_unique_id';
            $muid = strtoupper($muid);
        } else {
            return DB_EMPTY_SET;
        }

        $query = "SELECT szbuniq_pidm"
            . " FROM szbuniq"
            . " WHERE $field = ?";

        $pidm = $this->dbh->queryfirstcolumn($query, $muid);

        return $pidm;
    }

    /**
     * @param array $muids
     * @return array
     */
    public function getPidms(array $muids): array
    {
        $pidmRegex = '/^\d{1,8}$/';
        $bannerIdRegex = '/^\+[0-9]{1,8}$/';
        $uniqueIdRegex = '/^\w{1,8}$/';

        $pidms = [];
        $bannerIds = [];
        $uniqueIds = [];

        foreach ($muids as $id) {
            if (preg_match($pidmRegex, $id)) {
                $pidms[] = $id;
            } elseif (preg_match($bannerIdRegex, $id)) {
                $bannerIds[] = $id;
            } elseif (preg_match($uniqueIdRegex, $id)) {
                $uniqueIds[] = strtoupper(trim($id));
            }
        }

        $where = [];

        if (!empty($pidms)) {
            $where[] = "szbuniq_pidm in (" . rtrim(str_repeat(" ?,", count($pidms)), ',') . ")";
        }

        if (!empty($bannerIds)) {
            $where[] = "szbuniq_banner_id in (" . rtrim(str_repeat(" ?,", count($bannerIds)), ',') . ")";
        }

        if (!empty($uniqueIds)) {
            $where[] = "szbuniq_unique_id in (" . rtrim(str_repeat(" ?,", count($uniqueIds)), ',') . ")";
        }

        if (empty($where)) {
            return [];
        }

        $query = "SELECT szbuniq_pidm"
            . " FROM szbuniq"
            . " WHERE " . implode(' OR ', $where);

        $params = array_merge($pidms, $bannerIds, $uniqueIds);

        $records = $this->dbh->queryall_array($query, $params);

        return array_column($records, 'szbuniq_pidm');;
    }


    /**
     * @param string $muid
     * @param string $examCode
     * @param string $examDate
     * @return DataResponse
     */
    public function deleteSingle(string $muid, string $examCode, string $examDate): DataResponse
    {
        $dataResponse = new DataResponse();

        try {
            $pidm = $this->getPidm($muid);

            if ($pidm === DB_EMPTY_SET) {
                throw new \Exception('Provided MUID does not match any pidm.');
            }

            $formatDateSQL = "TO_DATE(:examDate, 'yyyy-mm-dd\"T\"HH24:MI:SS')";

            $sql = "DELETE payroll.pprexam"
                . " WHERE pprexam_pidm = :pidm"
                . " AND pprexam_exam_code = :examCode"
                . " AND pprexam_exam_date = $formatDateSQL";

            $sth = $this->dbh->prepare($sql);

            $sth->bind_by_name('examCode', $examCode);
            $sth->bind_by_name('examDate', $examDate);
            $sth->bind_by_name('pidm', $pidm);

            $success = $sth->execute();

            if (!$success) {
                throw new \Exception('Failed to delete record.');
            }
        } catch (\Exception $e) {
            $dataResponse->addError($e->getMessage() . '. ' . $e->getTraceAsString());
        }

        return $dataResponse;
    }
}