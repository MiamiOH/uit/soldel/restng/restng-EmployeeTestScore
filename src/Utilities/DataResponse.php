<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 8/26/18
 * Time: 5:17 PM
 */

namespace MiamiOH\RestngEmployeeTestScore\Utilities;

class DataResponse
{
    private $hasError = false;
    private $errors = [];

    public function addError(string $error): void
    {
        $this->hasError = true;
        $this->errors[] = $error;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasError(): bool
    {
        return $this->hasError;
    }
}