<?php

namespace MiamiOH\RestngEmployeeTestScore\Utilities;

use Illuminate\Validation;
use Illuminate\Translation;
use Illuminate\Filesystem\Filesystem;

class Validator
{
    private $validatorFactory;

    private $rules = [];
    private $messages = [];
    private $customAttributes = [];

    public function __construct()
    {
        $this->validatorFactory = new Validation\Factory(
            $this->loadTranslator()
        );
    }

    protected function loadTranslator()
    {
        $langPath = dirname(dirname(__FILE__)) . '/Utilities/lang';

        $filesystem = new Filesystem();

        $loader = new Translation\FileLoader(
            $filesystem,
            $langPath
        );

        $loader->addNamespace(
            'lang',
            $langPath
        );

        $loader->load('en', 'validation', 'lang');

        return new Translation\Translator($loader, 'en');
    }

    public function validate(array $data): array
    {
        $errors = [];

        $validator = $this->validatorFactory->make(
            $data,
            $this->rules,
            $this->messages,
            $this->customAttributes);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
        }

        return $errors;
    }

    public function validateOptions(array $options): array
    {
        $errors = [];

        foreach ($options as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $element) {
                    $errors = array_merge(
                        $this->validate([$key => $element]),
                        $errors
                    );
                }
            } else {
                $errors = array_merge(
                    $this->validate([$key => $value]),
                    $errors
                );
            }
        }

        return $errors;
    }

    public function setRules(array $rules): void
    {
        $this->rules = $rules;
    }

    public function setMessages(array $messages): void
    {
        $this->messages = $messages;
    }

    public function setCustomAttributes(array $attributes): void
    {
        $this->customAttributes = $attributes;
    }
}