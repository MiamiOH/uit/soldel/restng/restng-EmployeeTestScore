<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 8/23/18
 * Time: 6:24 PM
 */

namespace MiamiOH\RestngEmployeeTestScore\Utilities;

use \MiamiOH\RESTng\Util\User;

class Authorization
{
    /**
     * @var User
     */
    private $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param $permList
     * @return bool
     * @throws \Exception
     */
    public function isAuthByKeys($permList)
    {
        if (!$this->user->isAuthorized('WebServices', 'EmployeeTestScore', $permList)) {
            return false;
        }
        return true;
    }

    // public function isAuthBySelf($uniqueID){
    //     $userName = $this->user()->getUsername();
    //     if(strlower($userName) !== strtolower($uniqueID)){
    //         return false;
    //     }
    //     return true;
    // }
}