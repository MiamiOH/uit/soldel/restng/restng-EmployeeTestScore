<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 8/23/18
 * Time: 6:21 PM
 */

namespace MiamiOH\RestngEmployeeTestScore\Utilities;

class DateTime
{
    public function __construct()
    {
        date_default_timezone_set('US/Eastern');
    }

    /**
     * @param string $dateTime
     * @return string
     * @throws \Exception
     */
    public function formatStandard(string $dateTime): string
    {
        try {
            $dateTimeObject = new \DateTime($dateTime);

            return $dateTimeObject->format('Y-m-d\TH:i:s');
        } catch (\Exception $e) {
            throw new \Exception(
                "Exam date is not valid. Date must be in format 'yyyy-mm-ddThh:mm:ss'. " .
                "DateTime is inserted as local time, not UTC. Oracle Date will not accept milliseconds. " .
                "Ensure to truncate the milliseconds part before submitting again."
            );
        }

    }
}