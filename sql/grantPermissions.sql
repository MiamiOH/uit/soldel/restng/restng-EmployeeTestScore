GRANT SELECT, INSERT, UPDATE, DELETE ON payroll.pprexam TO MUWS_GEN_RL;
GRANT SELECT, INSERT, UPDATE ON payroll.ptrexam TO MUWS_GEN_RL;
GRANT SELECT ON payroll.pebempl TO MUWS_GEN_RL;
GRANT SELECT ON general.goremal TO MUWS_GEN_RL;
GRANT SELECT ON saturn.spriden TO MUWS_GEN_RL;
GRANT SELECT ON saturn.szbuniq TO MUWS_GEN_RL;